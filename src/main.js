import Vue from 'vue';
import App from './App.vue';
import 'bulma/css/bulma.css';
import VueRouter from 'vue-router';
//import VueSocketIO from 'vue-socket.io'
import routes from './router';
const config = require('../config.json');
import { io } from 'socket.io-client';

//tell vue to use the router
Vue.use(VueRouter);

//create the router object
let router = new VueRouter({ routes: routes });

Vue.config.productionTip = false;

new Vue({
    router,
    mounted() {
        this.lastHighTime = Date.now();
        this.socket.on('connect', () => {
            console.log('connected');
            this.registerBinanceClient();
            this.syncData();
        });
        this.socket.on('disconnect', () => {
            this.consoleoutput.push('======================================');
            this.consoleoutput.push('disconnected from websockets');
            this.consoleoutput.push('======================================');
            this.registered = false;
            this.bitcoinBalance = 0;
        });
        this.socket.on('console', (msg) => {
            this.consoleoutput.push(msg);
            this.consoleScroller();
        });
        this.socket.on('armedStatus', (status) => {
            this.armed = status;
        });
        this.socket.on('multiplierStatus', (payload) => {
            this.buyMultiplier = payload.buyMultiplier;
            this.sellMultiplier = payload.sellMultiplier;
        });
        this.socket.on('bitcoinBalance', (balance) => {
            this.bitcoinBalance = parseFloat(balance);
        });
        this.socket.on('ackPrefs', (prefs) => {
            this.buyMultiplier = prefs.buyMultiplier;
            this.sellMultiplier = prefs.sellMultiplier;
            this.stopwatch = prefs.stopwatch;
            this.bailMultiplier = prefs.bailPriceMultiplier;
            this.triggerPoint = prefs.bailTriggerPercent;
        });
        this.socket.on('tradingStatus', (status) => {
            this.tradeActive = status;
        });
        this.socket.on('chosenCoinPrice', (price) => {
            this.chosenCoinPrice = price;
        });
        this.socket.on('targetPrice', (price) => {
            this.targetPrice = price;
        });
        this.socket.on('bailTriggerPrice', (price) => {
            console.log("GOT bailTriggerPrice PRICE " +price);
            this.bailTriggerPrice = price;
        });
        this.socket.on('bailPrice', (price) => {
            console.log("GOT BAIL PRICE " +price);
            this.bailPrice = price;
        });
        this.socket.on('buyInPrice', (price) => {
            console.log("GOT BUY IN PRICE " +price);
            this.buyInPrice = price;
        });
        this.socket.on('newHighTime', (time) => {
            console.log("GOT hightime " +time);
            this.lastHighTime = time;
        });
        this.socket.on('oneTimesPrice', (price) => {
            console.log("GOT oneTimesPrice " +price);
            this.oneTimesPrice = price;
        });
    },
    methods: {
        registerBinanceClient() {
            if (this.apiKey !== '' && this.secretKey !== '' && this.fullname !== '') {
                this.socket.emit('create', { apiKey: this.apiKey, secretKey: this.secretKey, name: this.fullname }, () => {
                    this.registered = true;
                    this.$router.push('/multiplier');
                });
            }
        },
        syncData() {
            let payload = {
                buyMultiplier: this.buyMultiplier,
                sellMultiplier: this.sellMultiplier,
                stopwatch: this.stopwatch,
                bailPriceMultiplier: this.bailMultiplier,
                bailTriggerPercent: this.triggerPoint,
            };
            this.socket.emit('setPrefs', payload);
        },
    },
    render: (h) => h(App),
    data() {
        return {
            socket: io(config.websocketLocation, { withCredentials: false }),
            bitcoinBalance: 0,
            tradeActive:false,
            consoleoutput: [],
            buyInPrice: 0,
            bailPrice:0,
            bailTriggerPrice:0,
            targetPrice:0,
            lastPrice: 0,
            stopwatch: 0,
            lastHighTime:0,
            triggerPoint: 0.95,
            bailMultiplier: 1,
            oneTimesPrice:1,
            armed: false,
            buyMultiplier: 1.5,
            sellMultiplier: 2.5,
            apiKey: '',
            secretKey: '',
            fullname: '',
            registered: false,
            chosenCoinPrice:0,
            consoleScroller: () => {},
        };
    },
}).$mount('#app');
