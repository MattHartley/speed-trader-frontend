module.exports = [
    {
        path: '/',
        name: 'home',
        component: () => import('./views/login'),
    },
    {
        path: '/console',
        name: 'console',
        component: () => import('./views/console'),
        props: true,
    },
    {
        path: '/multiplier',
        name: 'multiplier',
        component: () => import('./views/multiplier'),
        props: true,
    },
    {
        path: '/register',
        name: 'register',
        component: () => import('./views/register'),
        props: true,
    },
];
